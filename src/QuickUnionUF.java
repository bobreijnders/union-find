/****************************************************************************
 *  Compilation:  javac QuickUnionUF.java
 *  Execution:  java QuickUnionUF < input.txt
 *  Dependencies: StdIn.java StdOut.java 
 *  			  --> found at: http://introcs.cs.princeton.edu/java/stdlib/StdIn.java.html
 *
 *  Weighted Quick-union algorithm + Compression
 *
 ****************************************************************************/

public class QuickUnionUF {
	
	private int[] id;
	private int[] sz;
	
	/**
	 * Set the id of every node to itself
	 * @param N the number of nodes
	 */
	public QuickUnionUF(int N) {
		// 
		id = new int[N];
		sz = new int[N];
		
		 for (int i = 0; i < N; i++){
			 id[i] = i;
			 sz[i] = 1;
		 }
	}
	/**
	 * Connect node-p with node-q
	 * @param p
	 * @param q
	 */
	public void union(int p, int q){
		int i = root(p);
		int j = root(q);
		if (i == j) return;
		// add smallest sz tree to root of largest sz tree
		if (sz[i] < sz[j]) 	{ id[i] = j; sz[j] += sz[i]; }
		else 				{ id[j] = i; sz[i] += sz[j]; } 
	}
	/**
	 * Check whether two nodes are connected
	 * @param p node
	 * @param q node
	 * @return whether the nodes are connected
	 */
	public boolean connected(int p, int q){
		return root(p) == root(q);
	}
	/*
	 * Determine the Root node id
	 */
	private int root(int i){
		while(i != id[i]) {
			// path compression
			id[i] = id[id[i]];
			// set next i to current parent 
			i = id[i];
		}
		return i;
	}
	
	/*
	 * Application entry point
	 */
	public static void main(String[] args) {
		
		int N = StdIn.readInt();
		QuickUnionUF qu = new QuickUnionUF(N);
		
		// timing start
		Stopwatch stopwatch = new Stopwatch();
		
		while (!StdIn.isEmpty()) {
			int p = StdIn.readInt();
			int q = StdIn.readInt();
			
			if (!qu.connected(p, q)) {
				qu.union(p, q);
				StdOut.println(p + " " + q +" are now connected");
			}
		}
		
		// timing stop
		double time = stopwatch.elapsedTime();
		StdOut.println("Elapsed time: "+ time+" ms");
	}
}
