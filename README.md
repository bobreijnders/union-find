# README #
A code example of "Weighted quick-union with path compression". Find application examples at: [hofware.nl](http://www.hofware.nl/projects/union-find/)

Questions? Ask: Bob Reijnders ([contact](mailto:info@hofware.nl))

## Setup ##
Compilation:  javac QuickUnionUF.java

Execution:  java QuickUnionUF < input.txt

Dependencies: StdIn.java StdOut.java 

--> found at: http://introcs.cs.princeton.edu/java/stdlib/StdIn.java.html

###Weighted Quick-union algorithm + Compression###

 ****************************************************************************

## input.txt ##
* 10
* 3 4
* 2 9
* ...
* ..
* .

First line sets N, the number of nodes
Following lines connect node pairs